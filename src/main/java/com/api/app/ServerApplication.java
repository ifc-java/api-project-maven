package com.api.app;

import com.api.app.config.AppModule;
import com.api.test.TesteArray;
import io.dropwizard.Application;
import com.api.app.config.AppConfiguration;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.vyarus.dropwizard.guice.GuiceBundle;

import java.util.Locale;

public class ServerApplication extends Application<AppConfiguration> {

    private final Logger logger = LoggerFactory.getLogger(ServerApplication.class);
    private GuiceBundle guice;
    private AppModule appModule;
    private static TesteArray testeArray;

    public static void main(String[] args) throws Exception {
        Locale.setDefault(new Locale("pt", "BR"));
        System.setProperty("user.language", "pt");
        System.setProperty("user.country", "BR");
        System.setProperty("nashorn.args", "--no-deprecation-warning");

        System.out.println("TESTE");
        testeArray = new TesteArray();
        testeArray.Declaracao_Array();
        testeArray.TamanhoArray();
        new ServerApplication().run(args);
    }

    @Override
    public String getName() {
        return "api-project-maven";
    }

    @Override
    public void initialize(Bootstrap<AppConfiguration> bootstrap) {
        this.appModule = new AppModule();
        this.guice = GuiceBundle.builder()
                .enableAutoConfig("com.api.app")
                .modules(this.appModule)
                .printDiagnosticInfo()
                .build();
        bootstrap.addBundle(this.guice);
        bootstrap.addBundle(new SwaggerBundle<AppConfiguration>() {
            @Override
            protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(AppConfiguration configuration) {
                return configuration.getSwaggerBundleConfiguration();
            }
        });
    }

    @Override
    public void run(AppConfiguration appConfiguration, Environment environment) throws Exception {
    }
}
