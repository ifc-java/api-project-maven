package com.api.app.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.vyarus.dropwizard.guice.module.support.ConfigurationAwareModule;
import ru.vyarus.dropwizard.guice.module.support.DropwizardAwareModule;

public class AppModule extends DropwizardAwareModule<AppConfiguration> implements ConfigurationAwareModule<AppConfiguration> {

    private Logger logger = LoggerFactory.getLogger(AppModule.class);
    private AppConfiguration configuration;

    @Override
    public void setConfiguration(AppConfiguration configuration) {
        this.configuration = configuration;
    }
}
